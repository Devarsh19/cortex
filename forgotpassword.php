<?php
	include 'connect.php';
?>
<!DOCTYPE html>
<html>
<head>
<title>Cortex</title>
<meta charset="utf-8">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
</head>
<body id="top">
<div class="wrapper row1">
  <header id="header" class="clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><i class="icon-medkit"></i> <a href="main.html">Company</a></h1>
    </div>
    <!-- ################################################################################################ -->
        <nav id="mainav" class="fl_right">
      <ul class="clear">
        <li class="active"><a href="main.html">Home</a></li>
        <li><a href="#">FAQs</a>
        </li>
        <li><a href="aboutUs.html">About Us</a></li>

        <li><a href="help.html">Help</a></li>
      </ul>
    </nav>
    <!-- ################################################################################################ --> 
  </header> 
</div>
<div class="wrapper bgded" style="background-color: #d9e6f2">
	<div id="announce" class="clear">
		<div class="modal-body">
			<form method="post" action="">
		          <center>
		          <div>
		          <h3>Recover your Password</h3>
		          <label for="username" class="ui-hidden-accessible">Email:</label>
		          <input type="email" required name="email" id="email" placeholder="Email address">
		          </div>
		          <button type="submit" name="submit">Send Password</button>
		          <button><a href="main.html" >Login </a></button>
		          </center>
			</form>
		</div>	
	</div>
</div>
<!--PHP SCRIPT FOR EMAIL IS VAILD OR NOT  -->
<?php
	$conn = mysqli_connect('localhost','root','','demo') or die("unable to connect");
	$email = $_POST['email'];
	if(isset($_POST['submit'])){
	$sql = "SELECT * FROM reg_data WHERE email = '$email'";
	$result = mysqli_query($conn,$sql);
	$count = mysqli_num_rows($result);
	if($count==1){
		echo "Password has been send to your registered mail address";
	}
	else{
		echo "Email you entered is not match with our records";
	}
}
?>
<!--PHP SCRIPT FOR SENDING EMAIL TO USER -->
<?php 
	
	// Please specify your Mail Server - Example: mail.yourdomain.com.
ini_set("SMTP","ssl://smtp.gmail.com");

// Please specify an SMTP Number 25 and 8889 are valid SMTP Ports.
ini_set("smtp_port","25");

// Please specify the return address to use
ini_set('sendmail_from', 'devarsh96patel@gmail.com');

	if(isset($_POST['submit'])){
		$row = mysqli_fetch_assoc($result);
		$to = $row['email'];
		$password = $row['password'];
		$subject = "Your new Password";
		$msg = "Please use this password to login ".$password."And dont forget to change your password after you login";
		if(mail($to,$subject,$msg)){
			echo "your password has been send to your mail id";
		}
		else{
			echo "try again";
		}

	}
?>
<div class="wrapper row4">
  <footer id="footer" class="clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <h6 class="title">Aimbys Solutions</h6>
      <img class="imgl pad5 borderedbox" src="images/demo/about-img.jpg" alt="">
      <p>We provide software designing and development solutions in various technologies for web, mobile and desktop. We also provide IT infrastructure solutions for network and server management services and system integrations.
      </p>
      <p></p>
    </div>
    <div class="one_quarter">
      <h6 class="title">Contact Us</h6>
      <address class="push30">
        Aimbys Solutions<br>
        A-612, Titanium City Center, Nr. Shyamal Cross Roads,<br>
        Anandnagar Road, Satellite, Ahmedabad, Gujarat, India<br>
      </address>
      <ul class="nospace">
        <li class="push10"><i class="icon-time"></i> Mon. - Satur. 9:00am - 7:00pm</li>
        <li class="push10"><i class="icon-phone"></i>++91 79 4032 7893 </li>
        <li><i class="icon-envelope-alt"></i> info@aimbys.com</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">Stay Connected</h6>
      <ul class="faico clear">
        <li><a class="faicon-facebook" href="#"><i class="icon-facebook"></i></a></li>
        <li><a class="faicon-twitter" href="#"><i class="icon-twitter"></i></a></li>
        <li><a class="faicon-linkedin" href="#"><i class="icon-linkedin"></i></a></li>
        <li><a class="faicon-google-plus" href="#"><i class="icon-google-plus"></i></a></li>
        <li><a class="faicon-skype" href="#"><i class="icon-skype"></i></a></li>
        <li><a class="faicon-dropbox" href="#"><i class="icon-dropbox"></i></a></li>      
        <li><a class="faicon-tumblr" href="#"><i class="icon-tumblr"></i></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ --> 
  </footer>
</div>
<!-- ################################################################################################ --> 
<!-- ################################################################################################ --> 
<!-- ################################################################################################ -->
	<div class="wrapper row5">
	  <div id="copyright" class="clear"> 
	    <!-- ################################################################################################ -->
	    <p class="fl_left">Copyright &copy; 2017 - All Rights Reserved - <a href="#">Domain Name</a></p>
	    <!--<p class="fl_right">Template by <a target="_blank" href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>-->
	    <!-- ################################################################################################ --> 
	  </div>
	</div>	
</body>
